# FFXIV Gathering Nodes Tracker

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

It is heavily inspired by [FFXIV Gathering](https://www.ffxiv-gathering.com/), which I used as an excuse to learn something about React applications.

The application displays a dual clock in UTC time (LT) and in-game time (ET, Eorzea Time). ET is calculated from the LT timestamp multiplied by a factor of `20.5714285714`.

The application then displays a list of gathering resource nodes (related to both Mining and Botany professions) that gets dynamically sorted and highlighted following the current ET.
Each node has an activation hour, can activate every 12 or 24 hours, and stays active for a certain number of hours.
Active nodes are placed at the top of the list, with the active ones nearer to expiration placed first. Inactive nodes are sorted with the ones nearer to the next activation on top.

Nodes information is read from a static JSON file.

A filtering section filters the list based on various node attributes. All filters are applied in AND.

The user can switch between a light and a dark theme. Theming media queries are provided by [react-responsive](https://github.com/yocontra/react-responsive). The toggle itself uses [react-toggle](https://github.com/aaronshaf/react-toggle). Theme selection is synced between different open windows and saved using [use-local-storage-state](https://github.com/astoilkov/use-local-storage-state)

## Interesting Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.
You may also see any lint errors in the console.
