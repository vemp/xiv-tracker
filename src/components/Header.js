const Header = ({ title }) => {
    return (
        <header>
            <h1>{title}</h1>
        </header>
    )
}

Header.defaultProps = {
    title : "A live updating resource tracker for FFXIV Miners and Botanists. Browse all nodes below or apply some filters first."
}

export default Header