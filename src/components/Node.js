import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faSeedling, faGem, faScroll, faExclamation } from "@fortawesome/free-solid-svg-icons";
import { isNodeActive, nextActivation, isNodeUpcoming, expiresIn } from "../utils/NodeUtils";
import { formatHours } from "../utils/TimeUtils";

const Node = ({ nd, hour }) => {

  const active = isNodeActive(nd, hour);
  const upcoming = isNodeUpcoming(nd, hour);
  const next = nextActivation(nd, hour);
  const exp = expiresIn(nd, hour);

  const getNodeStatus = () => {
    return active ? 'active' : upcoming ? 'upcoming' : 'inactive';
  }

  const formatScrips = () => {
    if (nd.scrips === null) {
      return '-';
    }
    return nd.scrips.substring(0, nd.scrips.indexOf(' '))
  }

  const getScripsColor = () => {
    if (nd.scrips === null) {
      return 'scrips-none';
    }
    return nd.scrips.includes('W') ? 'scrips-white' : nd.scrips.includes('P') ? 'scrips-purple' : 'scrips-none';
  }

  return (
    <>
      <tr className={getNodeStatus()}>
        <td className={'icon-' + getNodeStatus()}>
          <FontAwesomeIcon icon={active ? faCheck : faExclamation} className="fa-fw" size="lg" />
        </td>
        <td>{nd.name}</td>
        <td>{nd.patch}</td>
        <td>{nd.level}</td>
        <td>{formatHours(nd.hour)}</td>
        <td>{nd.every + 'h'}</td>
        <td>{nd.duration + 'h'}</td>
        <td>{nd.location}</td>
        <td>{nd.teleport}</td>
        <td>{nd.type}</td>
        <td><FontAwesomeIcon icon={nd.job === 'Mining' ? faGem : faSeedling} /></td>
        <td className={getScripsColor()}>
          <FontAwesomeIcon icon={faScroll} />
          <span>{formatScrips()}</span>
        </td>
        <td>{exp > -1 ? 'Expires in ' + exp + 'h' : (next <= 3 ? 'Active in ' + next + 'h' : '')}</td>
      </tr>
    </>
  )
}

export default Node