import Checkbox from "./Checkbox";

const CheckboxGroup = ({ data, change, filter}) => {

  return (
    <fieldset className="checkbox-group">
    <legend>{data.grouplabel}:</legend>
    <ul className="checkbox-ul">
      {data.items.map((c) => (
        <Checkbox key={c.id}
        group={data.group}
        data={c}
        change={change}
        filter={filter}
        />
      ))}
    </ul>
  </fieldset>
)
}

export default CheckboxGroup