import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSquare, faCheck } from "@fortawesome/free-solid-svg-icons";

const Checkbox = ({ group, data, change, filter }) => {

  return (
    <li>
      <label htmlFor={data.id} className="custom-checkbox">
        <input id={data.id}
          name={group}
          type="checkbox"
          value={data.value}
          checked={filter[group].includes(data.value)}
          onChange={(e) => change(e)}
        />
        <span className="unchecked fa-stack">
          <FontAwesomeIcon icon={faSquare} className="fa-stack-1x" size="lg" />
        </span>
        <span className="checked fa-stack">
          <FontAwesomeIcon icon={faSquare} className="fa-stack-1x" size="lg" />
          <FontAwesomeIcon icon={faCheck} className="fa-stack-1x fa-inverse" />
        </span>
        <span className="checkbox-label">{data.label}</span>
      </label>

    </li>
  )
}

export default Checkbox