import React from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSeedling, faGem } from "@fortawesome/free-solid-svg-icons";

const Logo = () => {
  return (
    <div className="top-title">
      <FontAwesomeIcon icon={faSeedling} className="icon-valign" />
      <span>x|v</span>
      <span>tracker</span>
      <FontAwesomeIcon icon={faGem} className="icon-valign" />
    </div>
  )
}

export default Logo