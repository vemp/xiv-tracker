import CheckboxGroup from "./CheckboxGroup";

const MultiSearch = ({ multifilter, update }) => {

  const onMultiFilterChange = (e) => {
    update(e)
  }

  const checkboxes = {
    job: {
      group: "job",
      grouplabel: "Job",
      items: [
        {
          id: "job-mining",
          value: "mining",
          label: "Mining",
        },
        {
          id: "job-botany",
          value: "botany",
          label: "Botany",
        },
      ]
    },
    patch: {
      group: "patch",
      grouplabel: "Patch",
      items: [
        {
          id: "patch-20",
          value: "2.0",
          label: "2.0 (ARR)",
        },
        {
          id: "patch-30",
          value: "3.0",
          label: "3.0 (HW)",
        },
        {
          id: "patch-40",
          value: "4.0",
          label: "4.0 (SB)",
        },
        {
          id: "patch-50",
          value: "5.0",
          label: "5.0 (ShB)",
        },
        {
          id: "patch-60",
          value: "6.0",
          label: "6.0 (EW)",
        },
      ]
    },
    type: {
      group: "type",
      grouplabel: "Type",
      items: [
        {
          id: "type-normal",
          value: "normal",
          label: "Normal (ARR only)",
        },
        {
          id: "type-unspoiled",
          value: "unspoiled",
          label: "Unspoiled",
        },
        {
          id: "type-folklore",
          value: "folklore",
          label: "Folklore",
        },
        {
          id: "type-ephemeral",
          value: "ephemeral",
          label: "Ephemeral",
        },
      ]
    },
    scrips: {
      group: "scrips",
      grouplabel: "Scrips",
      items: [
        {
          id: "scrips-none",
          value: "none",
          label: "None",
        },
        {
          id: "scrips-white",
          value: "W",
          label: "White",
        },
        {
          id: "scrips-purple",
          value: "P",
          label: "Purple",
        },
      ]
    },
  }


  return (
    <div className="search">
      <div className="searchBar">
        <input id="search"
          name="itemName"
          type="text"
          placeholder="Search item name..."
          value={multifilter.itemName}
          onChange={(e) => onMultiFilterChange(e)}
        />
      </div>

      <CheckboxGroup data={checkboxes.job} change={onMultiFilterChange} filter={multifilter} />
      <CheckboxGroup data={checkboxes.patch} change={onMultiFilterChange} filter={multifilter} />
      <CheckboxGroup data={checkboxes.type} change={onMultiFilterChange} filter={multifilter} />
      <CheckboxGroup data={checkboxes.scrips} change={onMultiFilterChange} filter={multifilter} />

    </div>
  )
}

export default MultiSearch