import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClock } from "@fortawesome/free-solid-svg-icons";
import { EORZEAN_TIME, formatHour } from "../utils/TimeUtils";

const Clock = ({ now, clockLabel }) => {

  return (
    <div className="clock">
      <FontAwesomeIcon icon={faClock} className="icon-valign" />
      <span>{clockLabel}</span>
      <span className="clockTime">{formatHour(now)}</span>
    </div>
  )
}

Clock.defaultProps = {
  clockLabel: EORZEAN_TIME
}

export default Clock