import React from 'react'
import Clock from "./Clock";
import DarkModeToggle from './DarkModeToggle';
import Logo from "./Logo";


const Topbar = ({ time }) => {
  return (
    <div className="topbar">
      <div className="topbar-wrapper">
        <Logo />
        <Clock now={time} />
        <DarkModeToggle />
      </div>
    </div>
  )
}

export default Topbar