import Node from "./Node"
import React from "react";
import { isNodeActive, nextActivation, expiresIn } from "../utils/NodeUtils";

const Nodes = ({ nodes, eorzeaHour }) => {


  // comparator function to sort nodes
  // active nodes first, then in order of next active time around the clock
  // if both nodes are active, the one with less remaining duration goes first
  const sortByNextOccurring = (n1, n2) => {
    const n1a = isNodeActive(n1, eorzeaHour);
    const n2a = isNodeActive(n2, eorzeaHour);
    if (n1a && !n2a) {
      return -1;
    }
    if (n2a && !n1a) {
      return 1;
    }
    if (n1a && n2a) {
      return expiresIn(n1, eorzeaHour) - expiresIn(n2, eorzeaHour);
    }
    return nextActivation(n1, eorzeaHour) - nextActivation(n2, eorzeaHour);
  }

  return (
    <>
      <table id="nodes">
        <thead>
          <tr>
            <th scope="col" className="th-active">&nbsp;</th>
            <th scope="col" className="th-name">Item Name</th>
            <th scope="col" className="th-expansion">Patch</th>
            <th scope="col" className="th-level">Level</th>
            <th scope="col" className="th-start">Active</th>
            <th scope="col" className="th-every">Every</th>
            <th scope="col" className="th-for">For</th>
            <th scope="col" className="th-location">Location</th>
            <th scope="col" className="th-teleport">Aetheryte</th>
            <th scope="col" className="th-type">Type</th>
            <th scope="col" className="th-job">Job</th>
            <th scope="col" className="th-scrips">Scrips</th>
            <th scope="col" className="th-status">Info</th>
          </tr>
        </thead>
        <tbody>
          {nodes
            .sort(sortByNextOccurring)
            .map((n) => (
              <Node key={n.id}
                nd={n}
                hour={eorzeaHour}
              />
            ))}
        </tbody>
      </table>
    </>
  )
}

export default React.memo(Nodes);