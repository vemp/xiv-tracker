
const Footer = () => {
  return (
    <div className="footer">
        <p>© 2022 vemp.org. Made as a React exercise with heavy inspiration from <a href="https://www.ffxiv-gathering.com/">FFXIV Gathering</a>. </p>
    </div>
  )
}

export default Footer