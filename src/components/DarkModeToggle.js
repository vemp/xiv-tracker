import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoon, faSun } from "@fortawesome/free-solid-svg-icons";
import Toggle from 'react-toggle';
import 'react-toggle/style.css';
import { useColorScheme } from '../hooks/UseColorScheme';

const DarkModeToggle = () => {

  const { isDark, setIsDark } = useColorScheme();

  return (
    <div className='dark-toggle-flex'>
      <div className='dark-toggle-wrapper'>
      <Toggle
        className='dark-mode-toggle'
        checked={isDark}
        onChange={(event) => setIsDark(event.target.checked)}
        icons={{
          checked: <FontAwesomeIcon icon={faMoon} className="icon-valign" />,
          unchecked: <FontAwesomeIcon icon={faSun} className="icon-valign" />
        }}
        aria-label="Dark mode toggle"
      />
      </div>
    </div>
  )
}

export default DarkModeToggle