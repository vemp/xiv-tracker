import { useEffect, useMemo } from "react";
import { useMediaQuery } from 'react-responsive';
import useLocalStorageState from "use-local-storage-state";

export function useColorScheme() {
  
  const[isDark, setIsDark] = useLocalStorageState('colorScheme', {
    ssr: true,
   })

  const sysprefDarkmode = useMediaQuery(
    {
      query: '(prefers-color-scheme: dark)',
    },
    undefined,
  );


  const value = useMemo(() => isDark === undefined ? !!sysprefDarkmode : isDark
    , [isDark, sysprefDarkmode]
  )

  useEffect(() => {
    if (value) {
      document.body.classList.add('darkmode');
    } else {
      document.body.classList.remove('darkmode');
    }
  }, [value]);

  return {
    isDark: value,
    setIsDark,
  };

}