import { useEffect, useState, useMemo } from "react";
import { utcToEorzea, getEorzeaHour } from "./utils/TimeUtils";
import Header from "./components/Header";
import Nodes from "./components/Nodes";
import MultiSearch from "./components/MultiSearch";
import Topbar from "./components/Topbar";
import Footer from "./components/Footer";

var jsondata = require('./dbfull.json');

const App = () => {

  const [currentTime, setCurrentTime] = useState(Date.now());

  const updateCurrentTime = () => {
    setCurrentTime(() => Date.now());
    updateEorzeaTime();
    updateEorzeaHour();
  };

  useEffect(() => {
    const interval = setInterval(updateCurrentTime, 1000);
    return () => clearInterval(interval);
  })


  const [eorzeaCurrentTime, setEorzeaCurrentTime] = useState(utcToEorzea(Date.now()));
  const updateEorzeaTime = () => setEorzeaCurrentTime(() => utcToEorzea(currentTime));

  const [eorzeaHour, setEorzeaHour] = useState(getEorzeaHour(eorzeaCurrentTime));
  const updateEorzeaHour = () => setEorzeaHour(() => getEorzeaHour(eorzeaCurrentTime));

  const [nodes, setNodes] = useState([]);


  /*
  * data loading section
  */

  //on component mount, load data from the backend
  useEffect(() => {
    const getNodes = async () => {
      setNodes(jsondata)
    }
    getNodes()
  }, []) //pass empty dependency array to only run this once at component mount




  /*
  *   multifilter section
  */

  // nodes list multifilter
  const [multifilter, setMultifilter] = useState({
    itemName: "",
    job: ["mining", "botany"],
    patch: ["2.0", "3.0", "4.0", "5.0", "6.0"],
    type: ["normal", "unspoiled", "ephemeral", "folklore"],
    scrips: ["none", "W", "P"],
  })

  // update the state of the multifilter based on the type of input changed
  const updateMultiFilter = (e) => {
    const newValue = e.target.type === "text" ? e.target.value : e.target.type === "checkbox" ? handleCheckboxFilter(e) : "";
    setMultifilter({
      ...multifilter,
      [e.target.name]: newValue
    })
  }

  // if a checkbox has changed, push(add) or filter(remove) the corresponding value
  // to the array of filters corresponding to the checkbox's group
  const handleCheckboxFilter = (e) => {
    console.log(e);
    let filterArray = multifilter[e.target.name];
    const isChecked = e.target.checked;
    if (isChecked) {
      filterArray.push(e.target.value)
    } else {
      filterArray = filterArray.filter(o => o !== e.target.value);
    }
    return filterArray;
  }

  // apply the filter stack to the data retrieved from the REST endpoint
  // and memoize the resulting filtered array until either the data or the filter change
  const multifilteredData = useMemo(() => {
    return nodes
      .filter(n => n.name.toLowerCase().includes(multifilter.itemName))
      .filter(n => multifilter.job.includes(n.job.trim().toLowerCase()))
      .filter(n => multifilter.patch.includes(n.patch))
      .filter(n => multifilter.type.includes(n.type))
      .filter(n => (n.scrips === null && multifilter.scrips.includes("none")) ||
        multifilter.scrips.some(e => {
          return n.scrips !== null && n.scrips.includes(e)
        })
      );
  }, [nodes, multifilter]);



  return (
    <>
      <Topbar time={eorzeaCurrentTime} />
      <div className="main">
        <div className="wrapper">
          <Header />
          <MultiSearch multifilter={multifilter} update={updateMultiFilter} />
          <Nodes nodes={multifilteredData} eorzeaHour={eorzeaHour} />
        </div>
      </div>
      <Footer />
    </>
  );
}

export default App;
