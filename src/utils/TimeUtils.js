// conversion factor between UTC and Eorzea time
export const EORZEA_TIME_FACTOR = 20.5714285714;
export const EORZEAN_TIME = "ET";

// convert UTC timestamp to Eorzean timestamp
export function utcToEorzea(now) {
    return Math.floor(now * EORZEA_TIME_FACTOR);
}

// get only the hour value for ET
export function getEorzeaHour(et) {
    return new Date(et).getUTCHours();
}

// return a formatted String for the given timestamp
export function formatHour(ts) {
    // pad a single digit value with a leading zero
    const leadingZero = (num) => `0${num}`.slice(-2);
    const format = (d) => {
        return [d.getUTCHours(), d.getUTCMinutes()]
            .map(leadingZero)
            .join(':')
    }
    return format(new Date(ts));
}

export function formatHours(h) {
    const withZero = (h) => `0${h}`.slice(-2);
    return withZero(h) + ':00';
}
