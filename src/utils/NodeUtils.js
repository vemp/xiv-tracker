// check if node is currently active
export function isNodeActive(node, eorzeaHour) {
  // console.log("isnodeactive");
  const end = (node.hour + node.duration) % node.every;
  const currHour = node.every === 12 ? eorzeaHour % 12 : eorzeaHour;
  return (node.hour <= end) ? (currHour >= node.hour && currHour < end)
    : ((currHour >= node.hour && currHour >= end) || (currHour < node.hour && currHour < end))
}

// return how many hours left until next activation for given node
export function nextActivation(node, eorzeaHour) {
  const currHour = node.every === 12 ? eorzeaHour % 12 : eorzeaHour;
  if (node.hour >= currHour) {
    return node.hour - currHour;
  } else {
    return (node.every - currHour + node.hour)
  };
}

// check if node will become active next hour
export function isNodeUpcoming(node, eorzeaHour) {
  return nextActivation(node, eorzeaHour) === 1
}

// returns how many hours left before active node becomes inactive
// return -1 for inactive nodes
export function expiresIn(node, eorzeaHour) {
  if (!isNodeActive(node, eorzeaHour)) {
    return -1;
  }
  const end = (node.hour + node.duration) % node.every;
  const currHour = node.every === 12 ? eorzeaHour % 12 : eorzeaHour;
  if (end >= currHour) {
    return end - currHour;
  } else {
    return (node.every - currHour + end)
  };
}
